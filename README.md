# Magic Numbers (not the music group)

## In order to enjoy this program you should be able to do ...

```ruby
bundle install
```

And then in order to view the results

```ruby
rspec spec/
```

About the STDOUT evaluation, you get something like this

```ruby
"\"Bar\"\n"
```

And that's funny because STDOUT acts in misterious ways,
but I think it got the job done.

No ```if``` where used or harmed to the elaboration of this program.

