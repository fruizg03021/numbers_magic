require 'byebug'
class NumberPrinter
  def print_sequence(bottom=1, top=100)
    (bottom..top).each do |x|
      begin
        public_send("print_#{evaluator(x).class}".downcase)
      rescue
        puts x.to_s
      end
    end
  end

  def print_multiplethree
    p 'Foo'
  end

  def print_multiplefive
    p 'Bar'
  end

  def print_multipleboth
    p 'FooBar'
  end


  private

  def evaluator(number)
    Multiples.all.detect { |s| s.match?(number) }.new
  end
end

class Multiples
  def self.all
    [MultipleBoth, MultipleFive, MultipleThree]
  end
end

class MultipleFive < Multiples
  def self.match?(number)
    number % 5 == 0
  end
end

class MultipleThree < Multiples
  def self.match?(number)
    number % 3 == 0
  end
end

class MultipleBoth < Multiples

  def self.match?(number)
    number % 5 == 0 && number % 3 == 0
  end
end




