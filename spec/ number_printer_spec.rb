require 'rspec'
require 'number_printer'

describe 'NumberPrinter' do
  context 'when entering a correct number' do

    it 'returns an integer' do
      number_printer = NumberPrinter.new
      expect {number_printer.print_sequence(1,1) }.to output("1\n").to_stdout

    end

    it 'returns Foo' do
      number_printer = NumberPrinter.new
      expect {number_printer.print_sequence(3,3) }.to output("\"Foo\"\n").to_stdout
    end

    it 'returns Bar' do
      number_printer = NumberPrinter.new
      expect {number_printer.print_sequence(5,5) }.to output("\"Bar\"\n").to_stdout

    end

    it 'returns FooBar' do
      number_printer = NumberPrinter.new
      expect {number_printer.print_sequence(15,15) }.to output("\"FooBar\"\n").to_stdout

    end

    it 'returns Everything' do
      number_printer = NumberPrinter.new
      number_printer.print_sequence(1,100)
    end
  end
end